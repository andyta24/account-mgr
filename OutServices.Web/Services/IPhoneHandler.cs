﻿using OutServices.Web.EFModel;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public interface IPhoneHandler
    {
        int Create(Phone p);
        int Create(IEnumerable<Phone> p);
        IEnumerable<Phone> Read(string userId = null, string number = "", string type = "");
        int Update(Phone p);
        int Update(IEnumerable<Phone> p);
        int Remove(Phone p);
        int Remove(IEnumerable<Phone> p);
    }
}
