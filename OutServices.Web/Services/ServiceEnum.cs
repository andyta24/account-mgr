﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.Services.Repository
{
    internal enum EnumDALResult
    {
        Fail = 0,
        Complete = 1
    }
    /// <summary>
    /// DAL 中處裡的方式
    /// </summary>
    public enum EnumDALAction
    {
        Insert = 1,
        Remove = 2,
        Update = 3
    }
    internal struct UserExecuteResult
    {
        public List<dynamic> Result { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
    internal struct DALActionResult<T>
    {
        public T ModelName { get; set; }
        public EnumDALAction Action { get; set; }
        public int Result { get; set; }
    }
    /// <summary>
    /// OutServices.Web.Services.Repository Extension Methods
    /// </summary>
    public static class CodeExtension
    {
        /// <summary>
        /// Single to Collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        internal static IEnumerable<T> ToEnumerable<T>(this T name, T t) => new List<T> { t };
    }
}
