﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;


namespace OutServices.Web.Services.Async
{

    using OutServices.Web.Services.Repository.Async;
    using OutServices.Web.Services.Repository.Helper;
    using EFModel;
    public class UserAsyncHandler : IUserAsyncHandler
    {
        private const string Key = "MSSQLConnection";
        private static IDbConnection Connection => DbHelper.GetDbConnection(key: Key);
        public async Task<User> GetUserAsync(string id)
        {
            return await Connection.GetUserAsync(id);
        }
    }
}
