﻿using OutServices.Web.EFModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OutServices.Web.Services.Async
{

    using OutServices.Web.Services.Repository.Async;
    public class CodeAsyncHandler : ICodeAsyncHandler
    {
        public Task<int> CreateMaster(Master master) => CodeMasterAsyncDAL.Create(master);
        public Task<int> CreateMaster(IEnumerable<Master> master) => CodeMasterAsyncDAL.Creat(master);
        public Task<Master> ReadMaster(Master master) => CodeMasterAsyncDAL.Read(master);
        public Task<IEnumerable<Master>> ReadMasters(Master master,bool? isDelete) => CodeMasterAsyncDAL.Read(master, isDelete);
        public Task<int> RemoveMaster(Master master) => CodeMasterAsyncDAL.Remove(master);
        public Task<int> RemoveMaster(IEnumerable<Master> master) => CodeMasterAsyncDAL.Remove(master);
        public Task<int> UpdateMaster(Master master) => CodeMasterAsyncDAL.Update(master);
        public Task<int> UpdateMaster(IEnumerable<Master> master) => CodeMasterAsyncDAL.Update(master);


        public Task<int> CreateDetail(Detail detail)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateDetail(IEnumerable<Detail> detail)
        {
            throw new System.NotImplementedException();
        }

        public Task<Master> ReadDetail(Detail master)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<Detail>> ReadDetails(Detail detail, bool? isDelete)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateDetail(Detail master)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateDetail(IEnumerable<Detail> detail)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> RemoveDetail(Detail master)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> RemoveDetail(IEnumerable<Detail> detail)
        {
            throw new System.NotImplementedException();
        }

        
    }
}
