﻿using OutServices.Web.EFModel;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public interface IOrganizationHandler
    {
        int Create(Organization o);
        int Create(IEnumerable<Organization> o);
        IEnumerable<Organization> Read(string userId = null, string adCode = "", string type = "", string code = "");
        int Update(Organization o);
        int Update(IEnumerable<Organization> o);
        int Remove(Organization o);
        int Remove(IEnumerable<Organization> o);
    }
}
