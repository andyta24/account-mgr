﻿using OutServices.Web.EFModel;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public interface ISnsHandler
    {
        int Create(Sns s);
        int Create(IEnumerable<Sns> s);
        IEnumerable<Sns> Read(string userId = null, string type = "", string account = "");
        int Update(Sns s);
        int Update(IEnumerable<Sns> s);
        int Remove(Sns s);
        int Remove(IEnumerable<Sns> s);
    }
}
