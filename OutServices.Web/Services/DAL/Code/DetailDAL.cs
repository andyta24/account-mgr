﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;

namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Helper;
    
    public class CodeDetailDAL :BaseDAL
    {
        public CodeDetailDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        private static readonly ILog _log = LogManager.GetLogger(typeof(UserDAL));
        private static string _dbConnection;
        private const string _table = "[Code].[Detail]";
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        //-- Read Sql command
        private static readonly string _readSql = $@"SELECT 
                                                           [Code], 
                                                           [CodeSN], 
                                                           [Text], 
                                                           [Value], 
                                                           [Enabled], 
                                                           [UpdateDateTime]
                                                      FROM {_table}
                                                      WITH (NOLOCK)
                                                     WHERE 1=1";
        //--- Update Sql
        private static readonly string _updateSql = $@"UPDATE {_table}
                                                         SET [Text] = @Text
                                                            ,[Value] = @Value
                                                            ,[Enabled] = @Enabled
                                                            ,[UpdateDateTime] = getdate()
                                                       WHERE [Code] = @Code
                                                         AND [CodeSN] = @CodeSN";
        //--- Insert Sql
        private static readonly string _insertSql = $@"INSERT INTO {_table}(
                                                                  [Code]
                                                                 ,[Text]
                                                                 ,[Value]
                                                                 ,[Enabled]
                                                                 ,[IsDelete]
                                                                 ,[UpdateDateTime]
                                                                ) VALUES (
                                                                  @Code
                                                                 ,@Text
                                                                 ,@Value
                                                                 ,@Enabled
                                                                 ,0
                                                                 ,getdate()
                                                                )";
        //-- Remove Sql
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                         SET 
                                                             [IsDelete] = 1
                                                            ,[UpdateDateTime] = getdate()
                                                       WHERE [Code] = @Code
                                                         AND [CodeSN] = @CodeSN";
        public static Detail Read(Detail detail)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    if (detail?.Code != null)
                    {
                        sql += " AND [Code] = @Code";
                    }
                    Detail result = _conn.Query<Detail>(sql, detail).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static IEnumerable<Detail> Read(Detail master, bool? isDelete)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    if (master?.Code != null)
                    {
                        sql += @" AND [Code] = @Code";
                        sql += @" AND ([IsDelete] IS NULL OR [IsDelete] = 0)";
                    }
                    if (isDelete.HasValue)
                    {
                        bool enableFilter = master != null && (master.Enabled ?? false);
                        int enableValue = enableFilter ? 1 : 0;
                        sql += $@" AND [Enabled] = {enableValue}";
                    }
                    IEnumerable<Detail> result = _conn.Query<Detail>(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Create(Detail detail) => CreateProcess(detail.ToEnumerable(detail));
        public static int Create(IEnumerable<Detail> detail) => CreateProcess(detail);
        public static int Update(Detail detail) => UpdateProcess(detail.ToEnumerable(detail));
        public static int Update(IEnumerable<Detail> detail) => UpdateProcess(detail);
        public static int Remove(Detail detail) => RemoveProcess(detail.ToEnumerable(detail));
        public static int Remove(IEnumerable<Detail> detail) => RemoveProcess(detail);



        private static int CreateProcess(IEnumerable<Detail> detail)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, detail);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int RemoveProcess(IEnumerable<Detail> detail)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, detail);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int UpdateProcess(IEnumerable<Detail> detail)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updateSql;
                    int result = _conn.Execute(sql, detail);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}
