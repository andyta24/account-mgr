﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;

namespace OutServices.Web.Services.Repository
{

    using EFModel;
    using Helper;
    public class CityDAL:BaseDAL
    {
        private static string _dbConnection;
        private const string _table = "[Code].[City]";
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        public CityDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        public static IEnumerable<City> GetAllCitys(string code = "")
        {
            try
            {
                using (_conn)
                {
                    string sql = $@"SELECT  [CityCode]
                                           ,[AdCode]
                                           ,[Name]
                                           ,[Enabled]
                                           ,ISNULL(CityCode, '999') AS OrderKey 
                                      FROM {_table} 
                                     WHERE RIGHT(LEFT(AdCode,4),2)='00'
                                       AND [AdCode] <  '710000' 
                                       AND [AdCode] != '100000'";
                    const string orderSql = @"ORDER BY OrderKey,AdCode,CityCode";
                    if (!string.IsNullOrEmpty(code))
                    {
                        sql += " AND [Code] = @Code";
                    }
                    sql += orderSql;
                    var para = new { Code = code };
                    IEnumerable<City> result = _conn.Query<City>(sql, para);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
