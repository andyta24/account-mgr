﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;

namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Helper;
    public class CodeMasterDAL :BaseDAL
    {
        public CodeMasterDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        private static readonly ILog _log = LogManager.GetLogger(typeof(UserDAL));
        private static string _dbConnection;
        private const string _table = "[Code].[Master]";
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);

        private static readonly string _insertSql = $@"INSERT INTO {_table}(
                                                   [Code]
                                                  ,[CodeName]
                                                  ,[Category]
                                                  ,[Description]
                                                  ,[Enabled]
                                                  ,[IsDelete]
                                                  ,UpdateDateTime
                                                ) VALUES (
                                                   @Code
                                                  ,@CodeName
                                                  ,@Category
                                                  ,@Description
                                                  ,@Enabled
                                                  ,0
                                                  ,getdate()
                                                )";
        private static readonly string _readSql = $@"SELECT [Code]
                                                          ,[CodeName]
                                                          ,[Category]
                                                          ,[Description]
                                                          ,[Enabled]
                                                          ,[UpdateDateTime]
                                                      FROM {_table}
                                                     WHERE 1 = 1";
        private static readonly string _updateSql = $@"UPDATE {_table}
                                                         SET
                                                  [CodeName] = @CodeName
                                                 ,[Category] = @Category
                                              ,[Description] = @Description
                                                  ,[Enabled] = @Enabled
                                           ,[UpdateDateTime] = getdate()
                                                       WHERE [Code] = @Code";
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                         SET
                                                    [IsDelete] = 1
                                           ,[UpdateDateTime] = getdate()
                                                       WHERE [Code] = @Code";
        private static int CreateProcess(IEnumerable<Master> m)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, m);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int UpdateProcess(IEnumerable<Master> m)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updateSql;
                    int result = _conn.Execute(sql, m);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int RemoveProcess(IEnumerable<Master> m)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, m);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static Master Read(Master master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    if (master?.Code != null)
                    {
                        sql += " AND [Code] = @Code";
                    }
                    Master result = _conn.Query<Master>(sql, master).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static IEnumerable<Master> Read(Master master, bool? isDelete)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    if (master?.Code != null)
                    {
                        sql += @" AND [Code] = @Code";
                        sql += @" AND ([IsDelete] IS NULL OR [IsDelete] = 0)";
                    }
                    if (isDelete.HasValue)
                    {
                        bool enableFilter = master != null && (master.Enabled ?? false);
                        int enableValue = enableFilter ? 1 : 0;
                        sql += $@" AND [Enabled] = {enableValue}";
                    }
                    IEnumerable<Master> result = _conn.Query<Master>(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Create(Master master) => CreateProcess(master.ToEnumerable(master));
        public static int Create(IEnumerable<Master> master) => CreateProcess(master);
        public static int Update(Master master) => UpdateProcess(master.ToEnumerable(master));
        public static int Update(IEnumerable<Master> master) => UpdateProcess(master);
        public static int Remove(Master master) => RemoveProcess(master.ToEnumerable(master));
        public static int Remove(IEnumerable<Master> master) => RemoveProcess(master);
    }
}
