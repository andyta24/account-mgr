﻿using Dapper;
using log4net;
using OutServices.Web.Services.Repository.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OutServices.Web.Services.Repository
{
    using EFModel;

    public class AspNetUsersDAL : BaseDAL
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AspNetUsersDAL));
        private static string _dbConnection;
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        private const string _table = "[dbo].[AspNetUsers]";
        public AspNetUsersDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }

        public static IEnumerable<User> GetAllAspNetUsers(bool? hasChildren = false)
        {
            try
            {
                bool childrenProcess = hasChildren ?? false;
                using (_conn)
                {
                    string sql = $@"SELECT 
                                          main.Id,
                                          main.UserName UserId, 
                                          detail.First, 
                                          detail.NickName, 
                                          detail.Last, 
                                          detail.Enabled, 
                                          detail.IsDelete, 
                                          detail.CreateById, 
                                          detail.CreateDateTime,
                                          detail.UpdatedDateTime
                                          FROM {_table} main
                                LEFT JOIN [Authority].[User] detail 
                                       ON detail.Id = main.Id";
                    IEnumerable<User> result = _conn.Query<User>(sql);
                    if (!childrenProcess) return result;
                    //Todo:
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static AspNetUsers Read(string id = null, bool? hasChildren = false)
        {
            try
            {
                bool childrenProcess = hasChildren ?? false;
                using (_conn)
                {
                    string sql = $@"SELECT 
                                            [Id]
                                           ,[UserName]
                                           ,[Email]
                                           ,[PhoneNumber]
                                     FROM {_table}
                                    WHERE 1 = 1";
                    sql += !string.IsNullOrEmpty(id) ? $@" AND [Id] = {id}" : string.Empty;
                    AspNetUsers result = _conn.Query<AspNetUsers>(sql).FirstOrDefault();
                    if (!childrenProcess) return result;
                    if (result == null) return null;
                    IEnumerable<User> users = new List<User> { UserDAL.Read(id) };
                    result.User = users.ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}
