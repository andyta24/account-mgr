﻿namespace OutServices.Web.Services.Repository
{
    public abstract class BaseDAL
    {
        private static string _dbConnectionString { get; set; }
        protected BaseDAL(string dbconnectionString = "")
        {
            _dbConnectionString = string.IsNullOrEmpty(dbconnectionString) ? "MSSQLConnection" : dbconnectionString;
        }
        protected static string BaseDbConnectionString => _dbConnectionString;
    }
}
