﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutServices.Web.Services.Repository.Extension
{
    using EFModel;
    
    internal struct DALResult<T>
    {
        /// <summary>
        /// DAL 執行後結果的數值
        /// </summary>
        public T Result { get; set; }
        /// <summary>
        /// DAL 執行後的狀態
        /// </summary>
        public EnumDALResult Status { get; set; }
        /// <summary>
        /// DAL 執行後帶出來的訊息
        /// </summary>
        public string Message { get; set; }
    }
    public static class DALExtension
    {
        public static User SetUserInfo(this User user, EnumDALAction action)
        {
            if (user.GetNullCheck().Status == EnumDALResult.Fail) return null;
            //---
            if (action == EnumDALAction.Insert)
            {
                user = user.SetUserId();
            }

            UserExecuteResult SetUserInfoResult = new UserExecuteResult
            {
                Result = new List<object>(),
                StartTime = DateTime.Now
            };
            if (user.Sns != null && user.Sns.Any())
            {
                DALActionResult<Sns> snsResult = new DALActionResult<Sns>
                {
                    Result = user.Sns.SetSNSInfo(action),
                    Action = action
                };
                SetUserInfoResult.Result.Add(snsResult);
            }
            if (user.Phone != null && user.Phone.Any())
            {
                DALActionResult<Phone> phoneResult = new DALActionResult<Phone>
                {
                    Result = user.Phone.SetPhoneInfo(action),
                    Action = action
                };
                SetUserInfoResult.Result.Add(phoneResult);
            }
            if (user.Organization != null && user.Organization.Any())
            {
                DALActionResult<Organization> organizationResult = new DALActionResult<Organization>
                {
                    Result = user.Organization.SetOrganizationInfo(action),
                    Action = action
                };
                SetUserInfoResult.Result.Add(organizationResult);
            }
            SetUserInfoResult.EndTime = DateTime.Now;
            DALResult<UserExecuteResult> resultSetResult = new DALResult<UserExecuteResult>
            {
                Result = SetUserInfoResult,
                Status = EnumDALResult.Complete
            };
            return user;
        }

        private static User SetUserId(this User user)
        {
            string userId = user?.UserId;
            if (String.IsNullOrEmpty(userId)) return user;
            foreach (Phone p in user.Phone)
            {
                p.UserId = userId;
            }
            foreach (Organization o in user.Organization)
            {
                o.UserId = userId;
            }
            foreach (Sns s in user.Sns)
            {
                s.UserId = userId;
            }
            return user;
        }

        private static DALResult<int> GetNullCheck(this User user, EnumDALResult defaultStatus = EnumDALResult.Complete)
        {
            DALResult<int> setResult = new DALResult<int> { Status = defaultStatus };
            if (user == null)
            {
                setResult.Status = EnumDALResult.Fail;
                setResult.Message = "User Model is Empty";
            }
            else if (String.IsNullOrEmpty(user.UserId))
            {
                setResult.Status = EnumDALResult.Fail;
                setResult.Message = "UserId is Empty";
            }
            return setResult;
        }

        private static int SetSNSInfo(this IEnumerable<Sns> s, EnumDALAction action)
        {
            int result = -99;
            switch (action)
            {
                case EnumDALAction.Insert:
                    result = SnsDAL.Create(s);
                    break;
                case EnumDALAction.Update:
                    result = SnsDAL.Update(s);
                    break;
                case EnumDALAction.Remove:
                    result = SnsDAL.Remove(s);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            return result;
        }

        private static int SetPhoneInfo(this IEnumerable<Phone> p, EnumDALAction action)
        {
            int result = -99;
            switch (action)
            {
                case EnumDALAction.Insert:
                    result = PhoneDAL.Create(p);
                    break;
                case EnumDALAction.Update:
                    result = PhoneDAL.Update(p);
                    break;
                case EnumDALAction.Remove:
                    result = PhoneDAL.Remove(p);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            return result;
        }

        private static int SetOrganizationInfo(this IEnumerable<Organization> o, EnumDALAction action)
        {
            int result = -99;
            switch (action)
            {
                case EnumDALAction.Insert:
                    result = OrganizationDAL.Create(o);
                    break;
                case EnumDALAction.Update:
                    result = OrganizationDAL.Update(o);
                    break;
                case EnumDALAction.Remove:
                    result = OrganizationDAL.Remove(o);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            return result;
        }
    }
}
