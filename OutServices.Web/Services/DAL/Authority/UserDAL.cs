﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using OutServices.Web.Services.Repository.Helper;

namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Extension;
    public class UserDAL : BaseDAL
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UserDAL));
        private static string _dbConnection;
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        private const string _table = "[Authority].[User]";
        private static readonly string _insertSql = $@"INSERT INTO {_table}(
                                                                    [UserId]
                                                                   ,[Id]
                                                                   ,[NickName]
                                                                   ,[First]
                                                                   ,[Last]
                                                                   ,[Enabled]
                                                                   ,[IsDelete]
                                                                   ,[UpdatedDateTime]
                                                                   ,[CreateDateTime]
                                                                   ,[CreateById]
                                                                 ) VALUES (
                                                                    @UserId
                                                                   ,@Id
                                                                   ,@NickName
                                                                   ,@First
                                                                   ,@Last
                                                                   ,1
                                                                   ,0
                                                                   ,getdate()
                                                                   ,getdate()
                                                                   ,@CreateById
                                                                 );";

        private static readonly string _updateSql = $@"UPDATE {_table}
                                                          SET
                                                              [NickName] = @NickName
                                                             ,[First] = @First
                                                             ,[Last] = @Last
                                                             ,[Enabled] = 1
                                                             ,[IsDelete] 0
                                                             ,[UpdatedDateTime] = getdate()
                                                        WHERE [UserId] = @UserId";
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                          SET  [IsDelete] 1
                                                              ,[UpdatedDateTime] = getdate()
                                                         WHERE [UserId] = @UserId";
        public UserDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        public static IEnumerable<User> GetAllUsers(bool hasChildren = false)
        {
            try
            {
                using (_conn)
                {
                    string sql = $@"SELECT 
                                           [UserId], 
                                           [Id], 
                                           [NickName], 
                                           [First], 
                                           [Last], 
                                           [Enabled],
                                           [IsDelete],
                                           [UpdatedDateTime], 
                                           [CreateDateTime]
                                     FROM {_table}
                                    WHERE 1 = 1";
                    IEnumerable<User> result = _conn.Query<User>(sql);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static User Read(string userId = null, string id = null, bool? hasChildren = false)
        {
            try
            {
                bool childrenProcess = hasChildren ?? false;
                using (_conn)
                {
                    string sql = $@"SELECT 
                                           [UserId], 
                                           [Id], 
                                           [NickName], 
                                           [First], 
                                           [Last], 
                                           [Enabled],
                                           [IsDelete],
                                           [UpdatedDateTime], 
                                           [CreateDateTime]
                                     FROM {_table}
                                    WHERE 1 = 1";
                    sql += !string.IsNullOrEmpty(userId) ? $@" AND [UserId] = '{userId}'" : string.Empty;
                    sql += !string.IsNullOrEmpty(id) ? $@" AND [id] = '{id}'" : string.Empty;
                    User result = _conn.Query<User>(sql).FirstOrDefault();
                    if (childrenProcess)
                    {
                        if (result != null)
                        {
                            result = GetUserInfo(result);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static int Create(User user)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, user);
                    user = user.SetUserInfo(EnumDALAction.Insert);
                    _log.Info("Create User:" + user.UserId);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Create(IEnumerable<User> user)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, user);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Update(User user)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updateSql;
                    int result = _conn.Execute(sql, user);
                    user = user.SetUserInfo(EnumDALAction.Update);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public static int Update(IEnumerable<User> user)
        {
            try
            {
                using (_conn)
                {
                    string sql = $@"UPDATE {_table}
                                       SET
                                            [NickName] = @NickName
                                           ,[First] = @First
                                           ,[Last] = @Last
                                           ,[Enabled] = 1
                                           ,[IsDelete] 0
                                           ,[UpdatedDateTime] = getdate()
                                     WHERE UserId = @UserId";
                    int result = _conn.Execute(sql, user);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Remove(User user)
        {
            try
            {
                using (_conn)
                {
                    string sql = $@"UPDATE {_table}
                                       SET  [IsDelete] 1
                                           ,[UpdatedDateTime] = getdate()
                                     WHERE UserId = @UserId";
                    int result = _conn.Execute(sql, user);
                    user = user.SetUserInfo(EnumDALAction.Remove);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Remove(IEnumerable<User> user)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, user);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private static User GetUserInfo(User user)
        {
            string userId = user.UserId;
            if (!string.IsNullOrEmpty(userId))
            {
                IEnumerable<Sns> sns = SnsDAL.Read(userId);
                IEnumerable<Phone> phone = PhoneDAL.Read(userId);
                IEnumerable<Organization> organization = OrganizationDAL.Read(userId);
                user.Sns = sns == null ? new List<Sns>() : sns.ToList();
                user.Phone = phone == null ? new List<Phone>() : phone.ToList();
                user.Organization = organization == null ? new List<Organization>() : organization.ToList();
            }
            return user;
        }


    }
}
