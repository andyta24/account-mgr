﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Helper;
    public class SnsDAL : BaseDAL
    {
        public SnsDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        private static string _dbConnection;
        private static readonly ILog _log = LogManager.GetLogger(typeof(UserDAL));
        private const string _table = "[Authority].[SNS]";
        private const string codeType = "SNS";
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        private static readonly string _insertSql = $@"INSERT INTO {_table}
                                                                 (
                                                                   [UserId]
                                                                  ,[Type]
                                                                  ,[Account]
                                                                  ,[Enabled]
                                                                  ,[IsDelete]
                                                                  ,[UpdateDateTime]
                                                                 ) VALUES (
                                                                   @UserId
                                                                  ,@Type
                                                                  ,@Account
                                                                  ,@Enabled
                                                                  ,0
                                                                  ,getdate()
                                                                 )";
        private static readonly string _readSql = $@"SELECT 
                                                              S.[UserId]
                                                            , S.[SNSSn]
                                                            , S.[Type]
                                                            , S.[Account]
                                                            , S.[Enabled]
                                                            , S.[IsDelete]
                                                            , S.[UpdateDateTime]
                                                            , d.[Text] TypeText
                                                       FROM {_table} S
                                                  LEFT JOIN [Code].[Detail] d 
                                                         ON d.[CodeSN] = s.[Type]
                                                        AND d.[Code] = '{codeType}'
                                                      WHERE 1 = 1 ";
        private static readonly string _updatetSql = $@"UPDATE {_table}
                                                           SET
                                                             ,[Enabled] = @Enabled
                                                             ,[UpdateDateTime] = getdate() -- datetime
                                                        WHERE [UserId] = @UserId
                                                          AND [SNSSn] = @SNSSn
                                                          AND [Type] = @Type
                                                          AND [Account] = @Account";

        private static readonly string _removeSql = $@"UPDATE {_table}
                                                          SET [IsDelete] = 1
                                                            ,[UpdateDateTime] = getdate()
                                                       WHERE [UserId] = @UserId
                                                         AND [SNSSn] = @SNSSn
                                                         AND [Type] = @Type
                                                         AND [Account] = @Account";


        public static Sns Read(Sns sns)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    sql += @" AND [UserId] = @UserId
                              AND [Type] = @Type
                              AND [Account] = @Account";
                    Sns result = _conn.Query<Sns>(sql, sns).FirstOrDefault();
                    if (result != null)
                    {
                        result.User = UserDAL.Read(result.UserId);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static IEnumerable<Sns> Read(string userId = null, string type = "", string account = null)
        {
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    sql += !string.IsNullOrEmpty(userId) ? $@" AND [UserId] = @UserId" : string.Empty;

                    sql += !string.IsNullOrEmpty(type) ? $@" AND [Type] = @Type" : string.Empty;

                    sql += !string.IsNullOrEmpty(account) ? @" AND [Account] = @Account" : string.Empty;

                    var para = new { UserId = userId, Type = type, Account = account };
                    return _conn.Query<Sns>(sql, para);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        public static int Create(Sns s) => CreateProcess(s.ToEnumerable(s));
        public static int Create(IEnumerable<Sns> s) => CreateProcess(s);
        public static int Update(Sns s) => UpdateProcess(s.ToEnumerable(s));
        public static int Update(IEnumerable<Sns> s) => UpdateProcess(s);


        public static int Remove(Sns s) => RemoveProcess(s.ToEnumerable(s));

        public static int Remove(IEnumerable<Sns> s) => RemoveProcess(s);
        private static int CreateProcess(IEnumerable<Sns> s)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, s);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int RemoveProcess(IEnumerable<Sns> s)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, s);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int UpdateProcess(IEnumerable<Sns> s)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updatetSql;
                    int result = _conn.Execute(sql, s);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}
