﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;

namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Helper;
    public class OrganizationDAL : BaseDAL
    {
        private static string _dbConnection;
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        private const string _table = "[Authority].[Organization]";
        private const string codeType = "Organization";

        public OrganizationDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        private static readonly string _insertSql = $@"INSERT INTO {_table}
                                                                 (
                                                                   [UserId]
                                                                 , [AdCode]
                                                                 , [Type]
                                                                 , [Code]
                                                                 , [Enabled]
                                                                 , [IsDelete]
                                                                 , [Comment]
                                                                 , [UpdateDateTime]
                                                       ) VALUES (
                                                                   @UserId
                                                                 , @AdCode
                                                                 , @Type
                                                                 , @Code
                                                                 , 1
                                                                 , 0
                                                                 , @Comment
                                                                 , getdate()
                                                                 )";
        private static readonly string _readSql = $@"SELECT 
                                                           o.UserId
                                                         , o.AdCode
                                                         , o.[Type]
                                                         , o.Code
                                                         , o.Enabled
                                                         , o.IsDelete
                                                         , o.Comment
                                                         , o.UpdateDateTime
                                                         , c.[Name] CityName
                                                         ,d.[Text] TypeText  
                                                      FROM  {_table} o
                                                 LEFT JOIN [Code].[City] c 
                                                        ON c.AdCode = o.AdCode
                                                 LEFT JOIN Code.Detail d 
                                                        ON d.CodeSN = o.Type
                                                       AND d.Code = '{codeType}'
                                                     WHERE 1 = 1";
        private static readonly string _updatetSql = $@"UPDATE {_table}
                                                           SET
                                                              ,[Enabled] = @Enabled
                                                              ,[UpdateDateTime] = getdate()                                                        
                                                         WHERE [UserId] = @UserId
                                                           AND [Number] = @Number
                                                           AND [Type] = @Type";
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                          SET [IsDelete] = 1
                                                             ,[UpdateDateTime] = getdate()
                                                        WHERE [UserId] = @UserId
                                                          AND [AdCode] = @AdCode
                                                          AND [Type]   = @Type
                                                          AND [Code]   = @Code";
        private static int CreateProcess(IEnumerable<Organization> o)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, o);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private static int UpdateProcess(IEnumerable<Organization> o)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updatetSql;
                    int result = _conn.Execute(sql, o);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private static int RemoveProcess(IEnumerable<Organization> o)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, o);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //----
        public static IEnumerable<Organization> Read(string userId = null, string adCode = "", string type = "", string code = "")
        {
            if (string.IsNullOrEmpty(userId))
            {
                return null;
            }
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    sql += @" AND [UserId] = @UserId";
                    if (!string.IsNullOrEmpty(adCode))
                    {
                        sql += @" AND [AdCode] = @AdCode";
                    }
                    if (!string.IsNullOrEmpty(type))
                    {
                        sql += @" AND [Type] = @Type";
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        sql += @" AND [Code] = @Code";
                    }
                    var para = new { UserId = userId, AdCode = adCode, Type = type, Code = code };
                    IEnumerable<Organization> result = _conn.Query<Organization>(sql, para);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static int Create(Organization o) => CreateProcess(o.ToEnumerable(o));
        public static int Create(IEnumerable<Organization> o) => CreateProcess(o);
        public static int Update(Organization o) => UpdateProcess(o.ToEnumerable(o));
        public static int Update(IEnumerable<Organization> o) => UpdateProcess(o);
        public static int Remove(Organization o) => RemoveProcess(o.ToEnumerable(o));
        public static int Remove(IEnumerable<Organization> o) => RemoveProcess(o);
    }
}
