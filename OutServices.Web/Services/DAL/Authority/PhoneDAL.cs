﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using log4net;

namespace OutServices.Web.Services.Repository
{
    using EFModel;
    using Helper;
    public class PhoneDAL : BaseDAL
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(PhoneDAL));
        private static string _dbConnection;
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: _dbConnection);
        private const string _table = "[Authority].[Phone]";
        private const string codeType = "Phone";
        private static readonly string _insertSql = $@"INSERT INTO {_table}
                                                                 (
                                                                   [UserId]
                                                                  ,[Number]
                                                                  ,[Type]
                                                                  ,[Enabled]
                                                                  ,[IsDelete]
                                                                  ,[UpdateDateTime]
                                                                 ) VALUES (
                                                                   @UserId
                                                                  ,@Number
                                                                  ,@Type
                                                                  ,@Enabled
                                                                  ,0
                                                                  ,getdate()
                                                                 )";
        private static readonly string _readSql = $@"SELECT   p.UserId
                                                            , p.Number
                                                            , p.[Type]
                                                            , p.Enabled
                                                            , p.IsDelete
                                                            , p.UpdateDateTime
                                                            , d.[Text] TypeText
                                                       FROM {_table} p
                                                  LEFT JOIN Code.Detail d 
                                                         ON d.CodeSN = p.Type
                                                        AND d.Code = '{codeType}'
                                                      WHERE 1 = 1";



        private static readonly string _updatetSql = $@"UPDATE {_table}
                                                           SET
                                                              ,[Enabled] = @Enabled
                                                              ,[UpdateDateTime] = getdate()                                                        
                                                         WHERE [UserId] = @UserId
                                                           AND [Number] = @Number
                                                           AND [Type] = @Type";
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                          SET [IsDelete] = 1
                                                             ,[UpdateDateTime] = getdate()
                                                        WHERE [UserId] = @UserId
                                                          AND [Number] = @Number
                                                          AND [Type] = @Type";
        public PhoneDAL()
        {
            _dbConnection = BaseDbConnectionString;
        }
        
        private static int CreateProcess(IEnumerable<Phone> p)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = _conn.Execute(sql, p);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int UpdateProcess(IEnumerable<Phone> p)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updatetSql;
                    int result = _conn.Execute(sql, p);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        private static int RemoveProcess(IEnumerable<Phone> p)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = _conn.Execute(sql, p);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
        }
        //----
        public static IEnumerable<Phone> Read(string userId = null, string number = "", string type = "")
        {
            if (string.IsNullOrEmpty(userId))
            {
                return null;
            }
            try
            {
                using (_conn)
                {
                    string sql = _readSql;
                    sql += @" AND [UserId] = @UserId";
                    if (!string.IsNullOrEmpty(number))
                    {
                        sql += @" AND [Number] = @Number";
                    }
                    if (!string.IsNullOrEmpty(type))
                    {
                        sql += @" AND [Type] = @Type";
                    }
                    var para = new { UserId = userId, Number = number, Type = type };
                    IEnumerable<Phone> result = _conn.Query<Phone>(sql, para);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static int Create(Phone phone) => CreateProcess(phone.ToEnumerable(phone));
        public static int Create(IEnumerable<Phone> phone) => CreateProcess(phone);
        public static int Update(Phone phone) => UpdateProcess(phone.ToEnumerable(phone));
        public static int Update(IEnumerable<Phone> phone) => UpdateProcess(phone);
        public static int Remove(Phone phone) => RemoveProcess(phone.ToEnumerable(phone));
        public static int Remove(IEnumerable<Phone> phone) => RemoveProcess(phone);
    }
}
