﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{

    using EFModel;
    public interface IAspNetUserHandler
    {
        IEnumerable<User> GetAllAspNetUsers(bool? hasChildren = false);
        AspNetUsers Read(string id, bool? hasChildren = false);
    }
}
