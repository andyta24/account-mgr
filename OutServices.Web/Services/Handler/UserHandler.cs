﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{
    using EFModel;
    using Repository;

    public class UserHandler : IUserHandler
    {
        public IEnumerable<User> GetAllUser(bool hasChildren = false) => UserDAL.GetAllUsers(hasChildren);

        public int Create(User user) => UserDAL.Create(user);

        public int Create(IEnumerable<User> user) => UserDAL.Create(user);

        public int Update(User user) => UserDAL.Update(user);

        public int Update(IEnumerable<User> user) => UserDAL.Update(user);

        public int Remove(User user) => UserDAL.Remove(user);

        public int Remove(IEnumerable<User> user) => UserDAL.Remove(user);

        public User Read(string userId = "", string id = "", bool? hasChildren = false) => UserDAL.Read(userId, id, hasChildren);

    }
}
