﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{
    using EFModel;
    using Repository;

    public class SnsHandler : ISnsHandler
    {
        public int Create(Sns s) => SnsDAL.Create(s);

        public int Create(IEnumerable<Sns> s) => SnsDAL.Create(s);

        public IEnumerable<Sns> Read(string userId = null, string type = "", string account = "") => SnsDAL.Read(userId, type, account);

        public int Update(Sns s) => SnsDAL.Update(s);

        public int Update(IEnumerable<Sns> s) => SnsDAL.Update(s);

        public int Remove(Sns s) => SnsDAL.Remove(s);

        public int Remove(IEnumerable<Sns> s) => SnsDAL.Remove(s);
    }
}
