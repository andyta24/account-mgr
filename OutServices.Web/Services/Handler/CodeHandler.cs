﻿using OutServices.Web.EFModel;
using OutServices.Web.Services.Repository;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public class CodeHandler : ICodeHandler
    {
        public int CreateMaster(Master master) => CodeMasterDAL.Create(master);

        public int CreateMaster(IEnumerable<Master> master) => CodeMasterDAL.Create(master);

        public int UpdateMaster(Master master) => CodeMasterDAL.Update(master);

        public int UpdateMaster(IEnumerable<Master> master) => CodeMasterDAL.Update(master);

        public Master ReadMaster(Master master) => CodeMasterDAL.Read(master);

        public IEnumerable<Master> ReadMasters(Master master = null) => CodeMasterDAL.Read(master, null);


        public Detail ReadDetail(Detail detail) => CodeDetailDAL.Read(detail);

        public IEnumerable<Detail> ReadDetails(Detail detail, bool? getAll = null) => CodeDetailDAL.Read(detail, getAll);


        public int CreateDetail(Detail detail) => CodeDetailDAL.Create(detail);

        public int CreateDetail(IEnumerable<Detail> detail) => CodeDetailDAL.Create(detail);

        public int UpdateDetail(Detail detail) => CodeDetailDAL.Update(detail);

        public int UpdateDetail(IEnumerable<Detail> detail) => CodeDetailDAL.Update(detail);
        public int RemoveDetail(Detail detail) => CodeDetailDAL.Remove(detail);
        public int RemoveDetail(IEnumerable<Detail> detail) => CodeDetailDAL.Remove(detail);

        public int RemoveMaster(Master master) => CodeMasterDAL.Remove(master);

        public int RemoveMaster(IEnumerable<Master> master) => CodeMasterDAL.Remove(master);
    }
}
