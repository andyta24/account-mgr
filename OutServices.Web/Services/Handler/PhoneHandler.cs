﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{
    using EFModel;
    using Repository;

    public class PhoneHandler : IPhoneHandler
    {
        public int Create(Phone p) => PhoneDAL.Create(p);

        public int Create(IEnumerable<Phone> p) => PhoneDAL.Create(p);

        public IEnumerable<Phone> Read(string userId = null, string number = "", string type = "") => PhoneDAL.Read(userId, number, type);

        public int Update(Phone p) => PhoneDAL.Update(p);

        public int Update(IEnumerable<Phone> p) => PhoneDAL.Update(p);

        public int Remove(Phone p) => PhoneDAL.Remove(p);

        public int Remove(IEnumerable<Phone> p) => PhoneDAL.Remove(p);
    }
}
