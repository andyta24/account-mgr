﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OutServices.Web.EFModel;
using OutServices.Web.Services.Repository;

namespace OutServices.Web.Services
{
    public class AspNetUserHandler : IAspNetUserHandler
    {
        public IEnumerable<User> GetAllAspNetUsers(bool? hasChildren = false) =>
            AspNetUsersDAL.GetAllAspNetUsers();

        public AspNetUsers Read(string id, bool? hasChildren = false) => AspNetUsersDAL.Read(id, hasChildren);
    }
}
