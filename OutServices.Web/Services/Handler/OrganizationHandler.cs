﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{
    using EFModel;
    using Repository;
    public class OrganizationHandler : IOrganizationHandler
    {
        public int Create(Organization o) => OrganizationDAL.Create(o);

        public int Create(IEnumerable<Organization> o) => OrganizationDAL.Create(o);

        public IEnumerable<Organization> Read(string userId = null, string adCode = "", string type = "",
            string code = "") => OrganizationDAL.Read(userId, adCode, type, code);

        public int Remove(Organization o) => OrganizationDAL.Remove(o);

        public int Remove(IEnumerable<Organization> o) => OrganizationDAL.Remove(o);

        public int Update(Organization o) => OrganizationDAL.Update(o);

        public int Update(IEnumerable<Organization> o) => OrganizationDAL.Update(o);
    }
}
