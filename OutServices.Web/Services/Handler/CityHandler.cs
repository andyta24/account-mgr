﻿using OutServices.Web.EFModel;
using OutServices.Web.Services.Repository;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public class CityHandler : ICityHandler
    {
        public IEnumerable<City> GetAllCitys(string code = "") => CityDAL.GetAllCitys(code);
    }
}
