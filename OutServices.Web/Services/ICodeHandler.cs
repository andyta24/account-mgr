﻿using OutServices.Web.EFModel;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public interface ICodeHandler
    {
        Master ReadMaster(Master master);
        IEnumerable<Master> ReadMasters(Master master = null);
        int CreateMaster(Master master);
        int CreateMaster(IEnumerable<Master> master);
        int UpdateMaster(Master master);
        int UpdateMaster(IEnumerable<Master> master);
        int RemoveMaster(Master master);
        int RemoveMaster(IEnumerable<Master> master);


        Detail ReadDetail(Detail detail);
        IEnumerable<Detail> ReadDetails(Detail detail, bool? enabled = null);
        int CreateDetail(Detail detail);
        int CreateDetail(IEnumerable<Detail> detail);
        int UpdateDetail(Detail detail);
        int UpdateDetail(IEnumerable<Detail> detail);
        int RemoveDetail(Detail detail);
        int RemoveDetail(IEnumerable<Detail> detail);
    }
}
