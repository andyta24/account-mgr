﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OutServices.Web.Services
{
    using OutServices.Web.EFModel;
    public interface IUserAsyncHandler
    {
        Task<User> GetUserAsync(string id);
    }
}
