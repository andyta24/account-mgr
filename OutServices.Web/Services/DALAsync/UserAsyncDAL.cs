﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace OutServices.Web.Services.Repository.Async
{
    using EFModel;
    public static class UserAsyncDAL
    {
        public static async Task<User> GetUserAsync(this IDbConnection conn,string id)
        {
            using (conn)
            {
                string sql = @"SELECT [UserId]
                                 ,[Id]
                                 ,[UserName]
                                 ,[First]
                                 ,[Last]
                                 ,[Enabled]
                                 ,[UpdateDateTime]
                             FROM [Authority].[User]
                            WHERE 1=1";
                sql += !string.IsNullOrEmpty(id) ?" AND Id = @Id":string.Empty;
                var sqlPara = new { Id = id};
                return await conn.QuerySingleOrDefault(sql,sqlPara);
            }
        }
    }
}
