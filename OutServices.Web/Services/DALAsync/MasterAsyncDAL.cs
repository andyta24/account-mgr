﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace OutServices.Web.Services.Repository.Async
{

    using EFModel;
    using Helper;

    public class CodeMasterAsyncDAL
    {
        private const string Key = "MSSQLConnection";
        private const string _table = "[Code].[Master]";
        private static IDbConnection _conn => DbHelper.GetDbConnection(key: Key);
        private static readonly string _insertSql = $@"INSERT INTO {_table}(
                                                                    [Code]
                                                                   ,[CodeName]
                                                                   ,[Category]
                                                                   ,[Description]
                                                                   ,[Enabled]
                                                                   ,[IsDelete]
                                                                   ,UpdateDateTime
                                                                 ) VALUES (
                                                                    @Code
                                                                   ,@CodeName
                                                                   ,@Category
                                                                   ,@Description
                                                                   ,@Enabled
                                                                   ,0
                                                                   ,getdate()
                                                                 )";
        private static readonly string readSql = $@"SELECT [Code]
                                                          ,[CodeName]
                                                          ,[Category]
                                                          ,[Description]
                                                          ,[Enabled]
                                                          ,[UpdateDateTime]
                                                           FROM {_table}
                                                           WHERE 1 = 1";
        private static readonly string _updateSql = $@"UPDATE {_table}
                                                          SET
                                                              [CodeName] = @CodeName
                                                             ,[Category] = @Category
                                                             ,[Description] = @Description
                                                             ,[Enabled] = @Enabled
                                                             ,[UpdateDateTime] = getdate()
                                                        WHERE [Code] = @Code";
        private static readonly string _removeSql = $@"UPDATE {_table}
                                                          SET
                                                              [IsDelete] = 1
                                                             ,[UpdateDateTime] = getdate()
                                                        WHERE [Code] = @Code";


        //---
        public static async Task<Master> Read(Master master)
        {
            try
            {
                using (_conn)
                {
                    string sql = readSql;
                    if (master?.Code != null)
                    {
                        sql += " AND [Code] = @Code";
                    }
                    return await _conn.QueryFirstOrDefaultAsync<Master>(sql, master);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<IEnumerable<Master>> Read(Master master, bool? isDelete)
        {
            try
            {
                using (_conn)
                {
                    string sql = readSql;
                    if (master?.Code != null)
                    {
                        sql += @" AND [Code] = @Code";
                        sql += @" AND ([IsDelete] IS NULL OR [IsDelete] = 0)";
                    }
                    if (isDelete.HasValue)
                    {
                        bool enableFilter = master != null && (master.Enabled ?? false);
                        int enableValue = enableFilter ? 1 : 0;
                        sql += $@" AND [Enabled] = {enableValue}";
                    }
                    Task<IEnumerable<Master>> result = _conn.QueryAsync<Master>(sql, master);
                    return await result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Create(Master master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Creat(IEnumerable<Master> master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _insertSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Update(Master master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updateSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Update(IEnumerable<Master> master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _updateSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Remove(Master master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static async Task<int> Remove(IEnumerable<Master> master)
        {
            try
            {
                using (_conn)
                {
                    string sql = _removeSql;
                    int result = await _conn.ExecuteAsync(sql, master);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
