﻿using OutServices.Web.EFModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OutServices.Web.Services
{
    public interface ICodeAsyncHandler
    {
        Task<int> CreateMaster(Master master);
        Task<int> CreateMaster(IEnumerable<Master> master);
        Task<Master> ReadMaster(Master master);
        Task<IEnumerable<Master>> ReadMasters(Master master, bool? isDelete);
        Task<int> UpdateMaster(Master master);
        Task<int> UpdateMaster(IEnumerable<Master> master);
        Task<int> RemoveMaster(Master master);
        Task<int> RemoveMaster(IEnumerable<Master> master);

        Task<int> CreateDetail(Detail detail);
        Task<int> CreateDetail(IEnumerable<Detail> detail);
        Task<Master> ReadDetail(Detail master);
        Task<IEnumerable<Detail>> ReadDetails(Detail detail, bool? isDelete);
        Task<int> UpdateDetail(Detail master);
        Task<int> UpdateDetail(IEnumerable<Detail> detail);
        Task<int> RemoveDetail(Detail master);
        Task<int> RemoveDetail(IEnumerable<Detail> detail);
    }
}
