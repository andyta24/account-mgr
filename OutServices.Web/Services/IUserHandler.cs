﻿using System.Collections.Generic;

namespace OutServices.Web.Services
{
    using EFModel;
    public partial interface IUserHandler
    {
        /// <summary>
        /// 取得所有使用者帳號資訊
        /// </summary>
        /// <param name="hasChildren"></param>
        /// <returns></returns>
        IEnumerable<User> GetAllUser(bool hasChildren = false);
        User Read(string userId = "", string id = "", bool? hasChildren = false);
        int Create(User user);
        int Create(IEnumerable<User> user);
        int Update(User user);
        int Update(IEnumerable<User> user);
        int Remove(User user);
        int Remove(IEnumerable<User> user);
    }
}
