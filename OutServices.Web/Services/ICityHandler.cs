﻿using OutServices.Web.EFModel;
using System.Collections.Generic;

namespace OutServices.Web.Services
{
    public interface ICityHandler
    {
        IEnumerable<City> GetAllCitys(string code = "");
    }
}
