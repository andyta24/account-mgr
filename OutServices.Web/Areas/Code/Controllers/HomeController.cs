﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OutServices.Web.Areas.Code.Controllers
{
    [Area("Code")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}