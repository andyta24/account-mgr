﻿using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.Areas.Identity.Pages.Account
{
    public class InputModel
    {
        [Required(ErrorMessage = "請輸入登入帳號")]
        [Display(Name = "電子郵件/帳號/手機號碼", Prompt = "請輸入電子郵件/帳號/手機號碼")]
        public string Account { get; set; }

        [Required(ErrorMessage = "這是一個必須輸入的資料")]
        [Display(Name = "電子郵件/帳號/手機號碼", Prompt = "請輸入電子郵件/帳號/手機號碼")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "請輸入密碼")]
        [Display(Name = "密碼", Prompt = "請輸入密碼")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "記住密碼")]
        public bool RememberMe { get; set; }
        [Display(Name = "顯示密碼")]
        public bool ToggleDisplay { get; set; }
        public string UserName { get; set; }
    }
}
