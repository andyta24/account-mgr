﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OutServices.Web.ViewModel;

namespace OutServices.Web.Areas.Backend.Controllers
{
    [Area("Backend")]
    [Authorize]
    public abstract class BackendController : Controller
    {
        protected abstract UserManager<IdentityUser> _userManager { get; set; }

        [HttpPost]
        public abstract IActionResult RenderPartialView(RenderInfo data);

    }
}
