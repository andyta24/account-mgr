﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using OutServices.Web.ViewModel;

namespace OutServices.Web.Areas.Backend.Controllers
{
    public partial class HomeController : BackendController
    {
        protected sealed override UserManager<IdentityUser> _userManager { get; set; }
        public override IActionResult RenderPartialView(RenderInfo data)
        {
            throw new System.NotImplementedException();
        }

        public HomeController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ClaimsPrincipal currentUser = User;
            Task<IdentityUser> user = _userManager.GetUserAsync(currentUser);
            IdentityUser email = user.Result;
            return RedirectToAction("Management","Account");
        }
    }
}