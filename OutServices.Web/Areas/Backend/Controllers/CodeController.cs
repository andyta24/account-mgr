﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using OutServices.Web.Factory;
using OutServices.Web.Services;


namespace OutServices.Web.Areas.Backend.Controllers
{
    using EFModel;
    using Helper;
    using ViewModel;
    public partial class CodeController : BackendController
    {
        protected sealed override UserManager<IdentityUser> _userManager { get; set; }
        private static ICodeHandler _codeServices { get; set; }

        public CodeController(UserManager<IdentityUser> userManager)
        {
            _codeServices = CodeFactory.CodeServices();
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        public override IActionResult RenderPartialView(RenderInfo data)
        {
            Dictionary<string, RenderInfo> pageList = ViewHelper.GetRenderConfig("Code");
            RenderInfo renderValue = pageList.FirstOrDefault(x => x.Key == data.PageType).Value;
            switch (data.PageType)
            {
                case "List":
                    {
                        IEnumerable<Master> model = _codeServices.ReadMasters();
                        return PartialView(renderValue.PVName, model);
                    }
                case "Edit":
                    {
                        var condition = new Master { Code = data.RouteString };
                        Master result = _codeServices.ReadMaster(condition);
                        CodeViewModel model = new CodeViewModel { Master = result, ActionType = ActionType.Update };
                        return PartialView(renderValue.PVName, model);
                    }
                case "Insert":
                    {
                        CodeViewModel model = new CodeViewModel { Master = new Master(), ActionType = ActionType.Insert };
                        return PartialView(renderValue.PVName, model);
                    }
                default:
                    return PartialView(renderValue.PVName);
            }
        }

        [HttpPost]
        public IActionResult GetListDetail(Detail data)
        {
            IEnumerable<Detail> model = new List<Detail>();
            if (data!=null)
            {
                model = _codeServices.ReadDetails(data);
            }
            return PartialView("_ListDetail", model);
        }


        [HttpPost]
        public IActionResult SaveCodeMaster(CodeViewModel data)
        {
            string viewName = "_List";
            IEnumerable<Master> masterList = data.MasterList;
            switch (data.ActionType)
            {
                case ActionType.Insert:
                    {
                        
                        int result = _codeServices.CreateMaster(masterList);
                        IEnumerable<Master> model = _codeServices.ReadMasters();
                        return PartialView(viewName, model);
                    }
                case ActionType.Update:
                    {
                        int result = _codeServices.UpdateMaster(masterList);
                        IEnumerable<Master> model = _codeServices.ReadMasters();
                        return PartialView(viewName, model);
                    }
                default:
                    return Json(false);
            }
        }

        [HttpPost]
        public IActionResult SaveCodeDetail(IEnumerable<CodeViewModel> data)
        {
            Detail model = new Detail { Code = string.Empty };
            IEnumerable<Detail> renderData = new List<Detail>();
            foreach (CodeViewModel d in data)
            {
                Detail detail = d.DetailList.FirstOrDefault();
                if (detail != null)
                {
                    model.Code = detail.Code;
                    SaveCodeDetailProcess(d);
                }
            }
            //Todo:
            if (!string.IsNullOrEmpty(model.Code))
            {
                renderData = _codeServices.ReadDetails(model);
                return PartialView("_ListDetail", renderData);
            }
            return PartialView("_ListDetail", renderData);
        }
        private int SaveCodeDetailProcess(CodeViewModel data)
        {
            int result = 0;
            IEnumerable<Detail> dataList = data.DetailList;
            switch (data.ActionType)
            {
                case ActionType.Insert:
                    {
                        result = _codeServices.CreateDetail(dataList);
                        break;
                    }
                case ActionType.Remove:
                    {
                        dataList = dataList.Select(x => { x.Enabled = false; return x; });
                        result = _codeServices.RemoveDetail(dataList);
                        break;
                    }
                case ActionType.Update:
                    {
                        result = _codeServices.UpdateDetail(dataList);
                        break;
                    }
                case ActionType.Reload:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return result;
        }
    }
}