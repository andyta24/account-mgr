﻿using log4net;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace OutServices.Web.Areas.Backend.Controllers
{
    using EFModel;
    using Helper;
    using ViewModel;
    public partial class AccountController : BackendController
    {
        protected sealed override UserManager<IdentityUser> _userManager { get; set; }
        private IdentityUser _currentIdentityUser { get; set; }
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));

        public AccountController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult Management()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Management(AccountViewModel account)
        {
            return View();
        }

        [HttpPost]
        public override IActionResult RenderPartialView(RenderInfo data)
        {
            User UserData = new User { UserId = data.UserId };
            Dictionary<string, RenderInfo> pageList = ViewHelper.GetRenderConfig();
            RenderInfo renderValue = pageList.FirstOrDefault(x => x.Key == data.PageType).Value;
            string renderType = data.PageType;
            if (renderType == "List")
            {
                IEnumerable<User> aspNetUsers = DataHelper.GetAllAspNetUsers();
                return PartialView(renderValue.PVName, aspNetUsers);
            }
            else if (renderType == "Edit")
            {
                User editUser = DataHelper.ReadUser(data.Id);
                if (editUser != null)
                {
                    UserData = editUser;
                }
                else
                {
                    renderType = "Insert";
                }
            }

            //Todo:
            EditViewModel commonModel = new EditViewModel
            {
                ActionType = renderType == "Edit" ? ActionType.Update : ActionType.Insert,
                User = UserData
            };
            commonModel = commonModel.SetUserListData(data.Id);
            if (!string.IsNullOrEmpty(data.Id))
            {

            }
            return PartialView(renderValue.PVName, commonModel);
        }
        public IdentityUser GetIdentityUser(ClaimsPrincipal claimsPrincipal)
        {
            return _userManager.GetUserAsync(claimsPrincipal).Result;
        }

    }
}