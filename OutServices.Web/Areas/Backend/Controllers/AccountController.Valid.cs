﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
namespace OutServices.Web.Areas.Backend.Controllers
{
    using EFModel;
    using Services;
    public partial class AccountController
    {
        public enum ValidType
        {
            Sns = 0,
            Phone = 1
        }
        /// <summary>
        /// 檢查資料是否存在
        /// </summary>
        /// <param name="type"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult ValidExist(ValidType type, string data)
        {
            bool retuBool = !string.IsNullOrEmpty(data);
            if (type == ValidType.Phone)
            {
                IPhoneHandler phoneServices = new PhoneHandler();
                IEnumerable<Phone> retuValue = phoneServices.Read(number: data);
                if (retuValue == null)
                {
                    retuBool = true;
                }
                else
                {
                    retuBool = !retuValue.Any();
                }

            }
            else if (type == ValidType.Sns)
            {
                ISnsHandler snsServices = new SnsHandler();
                retuBool = !snsServices.Read(account: data).Any();
            }
            return Json(retuBool);
        }
    }
}