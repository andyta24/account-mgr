﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OutServices.Web.Areas.Backend.Controllers
{
    using EFModel;
    using Helper;
    using ViewModel;

    internal static class AccountExtension
    {
        /// <summary>
        /// User Entity to IdentityUser Entity , And Get Email
        /// </summary>
        /// <param name="userData"></param>
        /// <param name="code"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static IdentityUser UserToIdentityUser(this User userData, string code = "SNS", string value = "Email")
        {
            IdentityUser identityUser = new IdentityUser
            {
                UserName = userData.UserId
            };
            // email
            IEnumerable<Detail> detailData = DataHelper.GetDetails(code);
            int detailCodeSN = detailData.Where(x => string.Equals(x.Value, value, System.StringComparison.OrdinalIgnoreCase))
                .Select(x => x.CodeSn)
                .FirstOrDefault();
            IEnumerable<Sns> snsData = userData.Sns?.ToList();
            if (snsData == null) return identityUser;
            {
                foreach (Sns s in snsData)
                {
                    
                }
                Sns sns = userData.Sns.FirstOrDefault(x => x.Type == detailCodeSN.ToString());
                if (sns!=null)
                {
                    identityUser.Email = sns.Account;
                }
            }
            return identityUser;
        }
    }

    public partial class AccountController
    {
        [HttpPost]
        public async Task<ActionResult> PostAccount(EditViewModel data)
        {
            bool retuBool = false;
            ActionType actionType = data.ActionType;
            User user = data.User;
            //--
            ClaimsPrincipal currentUser = User;
            user.CreateById = _userManager.GetUserName(currentUser);
            switch (actionType)
            {
                case ActionType.Insert:
                    {
                        retuBool = await AddToAspNetUser(user);
                        break;
                    }

                case ActionType.Update:
                    {
                        break;
                    }
                case ActionType.Reload:
                    break;
                case ActionType.Remove:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return Json(retuBool);
        }
        
        private async Task<bool> AddToAspNetUser(User userData)
        {
            bool retuBool = false;
            if (!string.IsNullOrEmpty(userData.Id))
            {
                retuBool = DataHelper.AddUser(userData) > 0;
            }
            else
            {
                IdentityUser user = userData.UserToIdentityUser();
                const string password = "1qaz@WSX";
                IdentityResult result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    userData.Id = user.Id;
                    retuBool = DataHelper.AddUser(userData) > 0;
                }
            }
            return retuBool;
        }
    }
}