﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class User
    {
        public User()
        {
            Organization = new HashSet<Organization>();
            Phone = new HashSet<Phone>();
            Sns = new HashSet<Sns>();
        }

        public string UserId { get; set; }
        public string Id { get; set; }
        public string NickName { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public bool Enabled { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public string CreateById { get; set; }
        public DateTime? CreateDateTime { get; set; }

        public AspNetUsers IdNavigation { get; set; }
        public ICollection<Organization> Organization { get; set; }
        public ICollection<Phone> Phone { get; set; }
        public ICollection<Sns> Sns { get; set; }
    }
}
