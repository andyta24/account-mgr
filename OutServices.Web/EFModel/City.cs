﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class City
    {
        public City()
        {
            Organization = new HashSet<Organization>();
        }

        public string CityCode { get; set; }
        public string AdCode { get; set; }
        public string Name { get; set; }
        public bool? Enabled { get; set; }

        public ICollection<Organization> Organization { get; set; }
    }
}
