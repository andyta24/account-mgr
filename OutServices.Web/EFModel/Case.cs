﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class Case
    {
        public string Snovendor { get; set; }
        public string CorpCode { get; set; }
        public string CaseCodeVendor { get; set; }
        public string CaseCodeSystem { get; set; }
        public DateTime? DateTimeUpload { get; set; }
        public string RegistName { get; set; }
        public string RegistNumber { get; set; }
        public string ActualName { get; set; }
        public string ActualAddress { get; set; }
        public int? SurveyWay { get; set; }
        public string ContactPerson { get; set; }
        public string ContactWay { get; set; }
        public int? OperationStatus { get; set; }
        public decimal? RegistFunds { get; set; }
        public int? CountOfStaff { get; set; }
        public string MerchantsType { get; set; }
        public string BusinessRange { get; set; }
        public string Snsurl { get; set; }
        public DateTime? RegistDateTime { get; set; }
        public bool? Enabled { get; set; }
        public int? CaseStatus { get; set; }
    }
}
