﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.EFModel
{
    public partial class Master
    {
        public Master()
        {
            Detail = new HashSet<Detail>();
        }
        [Required]
        [Display(Name = "代碼", Prompt = "代碼")]
        public string Code { get; set; }
        [Required]
        [Display(Name = "代碼名稱", Prompt = "代碼名稱")]
        public string CodeName { get; set; }
        [Display(Name = "類別", Prompt = "類別")]
        public string Category { get; set; }
        [Display(Name = "代碼描述", Prompt = "代碼描述")]
        public string Description { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public virtual ICollection<Detail> Detail { get; set; }
    }
}
