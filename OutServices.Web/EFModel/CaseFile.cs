﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class CaseFile
    {
        public string CaseCodeSystem { get; set; }
        public string UploadFileName { get; set; }
        public DateTime? UpdateDateTime { get; set; }
    }
}
