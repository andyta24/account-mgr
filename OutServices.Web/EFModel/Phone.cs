﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class Phone
    {
        public string UserId { get; set; }
        public string Number { get; set; }
        public string Type { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public User User { get; set; }
    }
}
