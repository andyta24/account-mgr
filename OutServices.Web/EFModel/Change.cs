﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class Change
    {
        public Guid TicketId { get; set; }
        public int Action { get; set; }
        public int Type { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
