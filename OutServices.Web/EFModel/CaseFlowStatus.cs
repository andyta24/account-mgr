﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class CaseFlowStatus
    {
        public string CaseCodeSystem { get; set; }
        public string CaseCodeVendor { get; set; }
        public int CaseStatus { get; set; }
        public DateTime? UpdateDateTime { get; set; }
    }
}
