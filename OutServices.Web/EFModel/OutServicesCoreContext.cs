﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace OutServices.Web.EFModel
{
    public partial class OutServicesCoreContext : DbContext
    {
        public OutServicesCoreContext()
        {
        }

        public OutServicesCoreContext(DbContextOptions<OutServicesCoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Case> Case { get; set; }
        public virtual DbSet<CaseFile> CaseFile { get; set; }
        public virtual DbSet<CaseFlowStatus> CaseFlowStatus { get; set; }
        public virtual DbSet<Change> Change { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Detail> Detail { get; set; }
        public virtual DbSet<Master> Master { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<Phone> Phone { get; set; }
        public virtual DbSet<Sns> Sns { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Case>(entity =>
            {
                entity.HasKey(e => new { e.CaseCodeVendor, e.CaseCodeSystem });

                entity.ToTable("Case", "Case");

                entity.Property(e => e.CaseCodeVendor).HasMaxLength(128);

                entity.Property(e => e.CaseCodeSystem).HasMaxLength(256);

                entity.Property(e => e.ContactPerson).HasMaxLength(50);

                entity.Property(e => e.ContactWay).HasMaxLength(50);

                entity.Property(e => e.CorpCode).HasMaxLength(128);

                entity.Property(e => e.DateTimeUpload).HasColumnType("datetime");

                entity.Property(e => e.RegistDateTime).HasColumnType("datetime");

                entity.Property(e => e.RegistFunds).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Snovendor)
                    .HasColumnName("SNOVendor")
                    .HasMaxLength(128);

                entity.Property(e => e.Snsurl).HasColumnName("SNSUrl");
            });

            modelBuilder.Entity<CaseFile>(entity =>
            {
                entity.HasKey(e => e.CaseCodeSystem);

                entity.ToTable("CaseFile", "Case");

                entity.Property(e => e.CaseCodeSystem)
                    .HasMaxLength(256)
                    .ValueGeneratedNever();

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<CaseFlowStatus>(entity =>
            {
                entity.HasKey(e => e.CaseCodeSystem);

                entity.ToTable("CaseFlowStatus", "Case");

                entity.Property(e => e.CaseCodeSystem)
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.CaseCodeVendor)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Change>(entity =>
            {
                entity.HasKey(e => e.TicketId);

                entity.ToTable("Change", "Operation");

                entity.Property(e => e.TicketId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.AdCode);

                entity.ToTable("City", "Code");

                entity.Property(e => e.AdCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.CityCode).HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Detail>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.CodeSn });

                entity.ToTable("Detail", "Code");

                entity.Property(e => e.Code).HasMaxLength(20);

                entity.Property(e => e.CodeSn)
                    .HasColumnName("CodeSN")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Text).HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.Property(e => e.Value).HasMaxLength(20);

                entity.HasOne(d => d.CodeNavigation)
                    .WithMany(p => p.Detail)
                    .HasForeignKey(d => d.Code)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Detail__Code__14270015");
            });

            modelBuilder.Entity<Master>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("Master", "Code");

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.Category).HasMaxLength(10);

                entity.Property(e => e.CodeName).HasMaxLength(200);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.AdCode, e.Code, e.Type });

                entity.ToTable("Organization", "Authority");

                entity.Property(e => e.UserId).HasMaxLength(128);

                entity.Property(e => e.AdCode).HasMaxLength(10);

                entity.Property(e => e.Code).HasMaxLength(20);

                entity.Property(e => e.Type).HasMaxLength(20);

                entity.Property(e => e.Comment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.AdCodeNavigation)
                    .WithMany(p => p.Organization)
                    .HasForeignKey(d => d.AdCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Organizat__AdCod__45F365D3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Organization)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Organizat__UserI__1DB06A4F");
            });

            modelBuilder.Entity<Phone>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.Number, e.Type });

                entity.ToTable("Phone", "Authority");

                entity.Property(e => e.UserId).HasMaxLength(128);

                entity.Property(e => e.Number).HasMaxLength(30);

                entity.Property(e => e.Type).HasMaxLength(20);

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Phone)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Phone__UserId__1F98B2C1");
            });

            modelBuilder.Entity<Sns>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.Type, e.Account, e.Snssn });

                entity.ToTable("SNS", "Authority");

                entity.Property(e => e.UserId).HasMaxLength(128);

                entity.Property(e => e.Type).HasMaxLength(20);

                entity.Property(e => e.Account).HasMaxLength(20);

                entity.Property(e => e.Snssn)
                    .HasColumnName("SNSSn")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UpdateDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Sns)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNS__UserId__1EA48E88");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("User", "Authority");

                entity.Property(e => e.UserId)
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateById)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime");

                entity.Property(e => e.First)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.Last)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.NickName).HasMaxLength(256);

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("FK__User__Id__5FB337D6");
            });
        }
    }
}
