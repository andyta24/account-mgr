﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class Organization
    {
        public string UserId { get; set; }
        public string AdCode { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDelete { get; set; }
        public string Comment { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public City AdCodeNavigation { get; set; }
        public User User { get; set; }
    }
}
