﻿using System;
using System.Collections.Generic;

namespace OutServices.Web.EFModel
{
    public partial class Detail
    {
        public string Code { get; set; }
        public int CodeSn { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public bool? Enabled { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Master CodeNavigation { get; set; }
    }
}
