﻿/**
 * Async Render Partial View form controller
 * @param {any} url ajax URL
 * @param {any} data Pass Data
 * @param {any} renderPV renderPV
 * @param {any} callbacks callbacks callback function List
 * @returns {any} return fail
 */
function asyncRenderPV(url, data, renderPV, callbacks) {
    if (url === undefined || url === "") {
        return false;
    }
    const callback = function (callbackFuns) {
        if (callbackFuns === undefined || callbackFuns === null) return false;
        callbackFuns.forEach(function (f) {
            const func = f["func"];
            const args = f["args"];
            func.apply(null, args);
        });
        return true;
    };
    $.post({
        url: url,
        data: data,
        xhrFields: {
            onprogress: function () { }
        },
        success: function (r) {
            if (renderPV !== undefined && renderPV.length > 0) {
                renderPV.html(r);
            }
        }
    }).done(callback(callbacks));
    return undefined;
}

function messagebox(opt) {
    opt = opt === undefined ? {} : opt;
    const getBodyContent = function (text, textColor, icon, iconColor) {
        const iconContent = $("<i>").addClass("fa").addClass(icon).css({ "color": iconColor, "margin-right": "5px" })[0].outerHTML;
        const textContent = $("<span>").text(text).css({ "color": textColor, "letter-spacing": "1px" })[0].outerHTML;
        return iconContent + textContent;
    };
    const option = {
        Id: opt.Id === undefined ? "OutServicesMessage" : opt.Id,
        Size: opt.Size === undefined ? "modal-lg" : opt.Size,
        Title: opt.Title === undefined ? "訊息視窗標題" : opt.Title,
        Body: opt.Body === undefined ? "訊息內容" : opt.Body,
        Color : opt.Color === undefined ? "blue" : opt.Color,
        Action: opt.Action === undefined ? "show" : opt.Action ? "Show" : "hide",
        Icon: opt.Type === "Warning" ? "fa-exclamation-triangle" : "fa-info-circle",
        IconColor: opt.Type === "Warning" ? "red" : "yellow"
    };
    const $messageBox = $(`#${option.Id}`);
    if ($messageBox.length === 1) {
        const $modaldialog = $messageBox.find(".modal-dialog");
        //Change Size
        $modaldialog.each(function () {
            $(this).removeClass("modal-sm").removeClass("modal-lg").addClass(option.Size);
        });
        //Change Title
        $modaldialog.find(".modal-title").each(function () {
            $(this).html(option.Title);
        });
        //Change Body
        $modaldialog.find(".modal-body").each(function () {
            const bodyText = getBodyContent(option.Body, option.Color, option.Icon, option.IconColor);
            $(this).html(bodyText);
        });
        $messageBox.modal(option.Action);
    }
    return $messageBox;
}
/**
 * 把 HTML 物件轉換成 Javascript 物件
 * @param {Object<any>} o HTML Object
 * @param {string} t HTML tagName
 * @param {string} p property
 * @param {string} s Separator symbol
 * @param {Object<any>} option Separator symbol
 * @returns {any} retuv Value
 */
function getJsObjectModel(o, t, p, s, option) {
    const remoteValid = function (opt) {
        let validResult = true;
        $.post({
            url: opt.URL,
            data: opt,
            async: false,
            success: function (r) {
                validResult = r;
            }
        });
        return validResult;
    };
    const setErrorMessage = function (i, message) {
        message = message === undefined ? "必須填入資料" : message;
        const $message = $("<span/>");
        const $messageText = i.siblings("span[error-message]");
        if ($messageText.length === 0) {
            $message.attr("error-message", "").text(message);
            i.after($message);
        } else {
            $messageText.text(message);
        }
    };
    const $data = {};
    t = t === undefined ? "input,select" : t;
    p = p === undefined ? "name" : p;
    s = s === undefined ? "." : s;
    option = option === undefined ? { Field: "" } : option;
    let validModel = true;
    o.find("span[error-message]").remove();
    o.find(t).each(function () {
        const $node = $(this);
        const required = this.classList.contains("required");
        const name = this.getAttribute(p).split(s).pop();
        const haveToValid = name === option.Field;
        const value = this.value;
        const tagName = this.tagName;
        if (required && value === "") {
            setErrorMessage($node);
            validModel = false;
            return false;
        } else {
            const $item = {};
            if (tagName === "INPUT") {
                $item["Text"] = "";
            } else if (tagName === "SELECT")
            {
                const $option = $(this).find("option:selected").text();
                $item["Text"] = $option;
            }
            $item["Value"] = value;
            $data[name] = $item;
        }
        return true;
    });
    const retuValue = validModel ? $data : undefined;
    console.log("getJsObjectModel:",option, retuValue);
    return retuValue;
}
(function ($) {
    $.each(["show", "hide", "first"], function (i, ev) {
        const resetValue = function (o) {
            $(o).find("input:visible,select:visible").each(function () {
                this.value = "";
                $(this).siblings("span[error-message]").remove();
            });
        };
        const dataAttr = "panelEditItem";
        let el = $.fn[ev];
        $.fn[ev] = function () {
            if ($(this).data(dataAttr) === "Edit") {
                resetValue(this);
            }
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
    $('form input[name*="ToggleDisplay"]').on("change", function () {
        const checked = this.checked;
        const $dataRoot = $(this).parents("form");
        const $password = $dataRoot.find('[name="Input.Password"]');
        const type = checked ? "text" : "password";
        $password.prop("type", type);
    });
})($);