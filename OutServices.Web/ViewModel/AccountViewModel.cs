﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace OutServices.Web.ViewModel
{
    using EFModel;
    public class AccountViewModel : VModel
    {
        public override ActionType ActionType { get; set; }
        public User User { get; set; }
        public IEnumerable<User> UserList { get; set; }
        public Dictionary<string, RenderInfo> RenderInfo { get; set; }
        /// <summary>
        /// 檢查重複的欄位
        /// </summary>
        public Dictionary<string, string> ValidConf { get; set; }
    }

    public class EditViewModel : VModel
    {
        public sealed override ActionType ActionType { get; set; }
        public RenderInfo RenderInfo { get; set; }
        public IEnumerable<SelectListItem> CityItems { get; set; }
        public IEnumerable<SelectListItem> PhoneType { get; set; }
        public IEnumerable<SelectListItem> OrganiztionType { get; set; }
        public IEnumerable<SelectListItem> SNSType { get; set; }
        public User User { get; set; }
        /// <summary>
        /// 系統是否有資料 / 區別 AspNetUsers
        /// </summary>
        public bool SystemData { get; set; }
    }
}
