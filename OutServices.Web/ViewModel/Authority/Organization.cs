﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(OrganizationMetaData))]
    public partial class Organization
    {
        /// <summary>
        /// 類別名稱
        /// </summary>
        public string TypeText { get; set; }
        /// <summary>
        /// 城市名稱
        /// </summary>
        public string CityName { get; set; }
    }

    public class OrganizationMetaData
    {
        [Required]
        [Display(Name = "使用者 Id")]
        public string UserId { get; set; }
        [Required]
        [Display(Name = "地區")]
        public string AdCode { get; set; }
        [Required]
        [Display(Name = "類別")]
        public int Type { get; set; }
        [Required]
        [Display(Name = "代碼")]
        public string Code { get; set; }
        
        [Display(Name = "註解說明")]
        public string Comment { get; set; }
        [Display(Name = "啟用")]
        public bool? Enabled { get; set; }
    }
}

