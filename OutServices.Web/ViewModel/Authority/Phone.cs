﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(PhoneMetaData))]
    public partial class Phone
    {

        public string TypeText { get; set; }
    }

    public class PhoneMetaData
    {
        [Required]
        [Display(Name = "使用者帳號 Id")]
        public string UserId { get; set; }
        [Required]
        [Display(Name = "類別")]
        public int Type { get; set; }
        [Required]
        [Phone]
        [Display(Name = "號碼")]
        public string Number { get; set; }
        [Display(Name = "啟用")]
        public bool? Enabled { get; set; }
        public bool? IsDelete { get; set; }
        

    }
}

