﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(SnsMetaData))]
    public partial class Sns
    {
        public string TypeText { get; set; }
    }

    public class SnsMetaData
    {
        [Required]
        [Display(Name = "使用者 Id")]
        public string UserId { get; set; }
        [Required]
        [Display(Name = "類別")]
        public int Type { get; set; }
        [Required]
        [Display(Name = "帳號")]
        public string Account { get; set; }
        [Display(Name = "啟用")]
        public bool? Enabled { get; set; }
    }
}

