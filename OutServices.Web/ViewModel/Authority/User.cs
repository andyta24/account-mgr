﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(UserMetaData))]
    public partial class User
    {

    }

    public class UserMetaData
    {
        /// <summary>
        /// Authority.User - > UID
        /// </summary>
        [Required]
        [Display(Name = "帳號")]
        public string UserId { get; set; }
        /// <summary>
        /// AspNetUser -> Id
        /// </summary>
        public string Id { get; set; }
        [Required]
        [Display(Name = "暱稱")]
        public string NickName { get; set; }
        [Required]
        [Display(Name = " 名 ")]
        public string First { get; set; }
        [Required]
        [Display(Name = " 姓 ")]
        public string Last { get; set; }
        [Display(Name = "啟用")]
        public bool Enabled { get; set; }
    }
}

