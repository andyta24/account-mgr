﻿namespace OutServices.Web.ViewModel
{
    /// <summary>
    /// Render 需要的配置數據
    /// </summary>
    public class RenderInfo
    {
        public int RouteValue { get; set; }
        public string RouteString { get; set; }
        public string MethodType { get; set; }
        public string Area { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string PVName { get; set; }
        #region PassValue From Web
        public string PageType { get; set; }
        public ActionType ActionType { get; set; }
        public string UserId { get; set; }
        public string Id { get; set; }
        #endregion

    }
}