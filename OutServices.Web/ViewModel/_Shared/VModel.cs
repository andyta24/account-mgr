﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutServices.Web.ViewModel
{
    public enum ActionType
    {
        Reload = 0,
        Insert = 1,
        Remove = 2,
        Update = 3,
    }
    public abstract class VModel
    {
        public abstract ActionType ActionType { get; set; }
    }
}
