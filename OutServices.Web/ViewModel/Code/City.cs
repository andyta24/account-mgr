﻿using Microsoft.AspNetCore.Mvc;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(DetailMetaData))]
    public partial class City { }

    public class CityMetaData { }
}
