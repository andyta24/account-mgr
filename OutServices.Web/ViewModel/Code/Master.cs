﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(MasterMetaData))]
    public partial class Master
    {
    }

    public class MasterMetaData
    {
        [Required]
        [Display(Name = "代碼")]
        public string Code { get; set; }
        [Display(Name = "代碼名稱")]
        public string CodeName { get; set; }
        [Display(Name = "分類代碼")]
        public string Category { get; set; }
        [Display(Name = "描述說明")]
        public string Description { get; set; }
        [Display(Name = "是否啟用")]
        public bool? Enabled { get; set; }
        [Display(Name = "更新時間")]
        public DateTime? UpdateDateTime { get; set; }
    }
}
