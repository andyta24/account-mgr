﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OutServices.Web.EFModel
{
    [ModelMetadataType(typeof(DetailMetaData))]
    public partial class Detail
    {

    }

    public class DetailMetaData
    {
        [Required]
        [Display(Name = "代碼")]
        public string Code { get; set; }
        [Required]
        [Display(Name = "流水編號")]
        public int CodeSn { get; set; }
        [Display(Name = "顯示文字",Prompt = "顯示文字")]
        public string Text { get; set; }
        [Display(Name = "數值",Prompt = "數值")]
        public string Value { get; set; }
        [Display(Name = "是否啟用")]
        public bool? Enabled { get; set; }
        [Display(Name = "更新時間")]
        public DateTime? UpdateDateTime { get; set; }
    }
}
