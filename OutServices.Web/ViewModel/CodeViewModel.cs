﻿using System.Collections.Generic;

namespace OutServices.Web.ViewModel
{
    using EFModel;
    public class CodeViewModel : VModel
    {
        public override ActionType ActionType { get; set; }
        public Master Master { get; set; }
        public Detail Detail { get; set; }

        public IEnumerable<Master> MasterList { get; set; }
        public IEnumerable<Detail> DetailList { get; set; }
        public Dictionary<string, RenderInfo> RenderInfo { get; set; }
    }
}
