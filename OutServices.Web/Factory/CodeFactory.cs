﻿using OutServices.Web.Services;

namespace OutServices.Web.Factory
{
    public class CodeFactory
    {
        public static ICodeHandler CodeServices() => new CodeHandler();
    }
}
