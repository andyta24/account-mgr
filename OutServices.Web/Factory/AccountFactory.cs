﻿using OutServices.Web.Services;

namespace OutServices.Web.Factory
{
    public class AccountFactory
    {
        private static UserHandler _userHandler;
        private static SnsHandler _snsHandler;
        private static PhoneHandler _phoneHandler;
        private static OrganizationHandler _organizationHandler;
        public AccountFactory()
        {
            _userHandler = new UserHandler();
            _snsHandler = new SnsHandler();
            _phoneHandler = new PhoneHandler();
            _organizationHandler = new OrganizationHandler();
        }

        public static IUserHandler UserServices => _userHandler;
        public static ISnsHandler UserSNSServices => _snsHandler;
        public static IPhoneHandler UserPhoneServices => _phoneHandler;
        public static IOrganizationHandler UserOrganizationServices => _organizationHandler;
    }
}
