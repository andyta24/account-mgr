﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OutServices.Web.Controllers
{
    public partial class CaseController : Controller
    {
        public async Task<IActionResult> Index()
        {
            await Task.Run(() => ShowThreadInfo("Task"));
            return View();
        }
        public IActionResult List()
        {
            IEnumerable<string> model = GetJSONSeed();
            return View(model);
        }

        private static void ShowThreadInfo(string s)
        {
            Console.WriteLine("{0} Thread ID: {1}", s, Thread.CurrentThread.ManagedThreadId);
        }

        private IEnumerable<string> GetJSONSeed()
        {
            List<string> returnValue = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                OutSeed item = new OutSeed
                {
                    Number = 12,
                    DateTime = DateTime.Now,
                    TrueFalse = true,
                    ArraySeed = new List<InnerSeed>
                    {
                        new InnerSeed
                        {
                            Content = "Content1",
                            Num = 14
                        },
                        new InnerSeed
                        {
                            Content = "Content2",
                            Num = 785
                        },
                        new InnerSeed
                        {
                            Content = "Content3",
                            Num = 325
                        },
                        new InnerSeed
                        {
                            Content = null,
                            Num = 325
                        }
                    }
                };
                string jsonStr = JsonConvert.SerializeObject(item);
                returnValue.Add(jsonStr);
                returnValue.Add(null);
            }
            returnValue.Add("xxxxx aaaaa cccc dddd");
            return returnValue;
        }
        private struct OutSeed
        {
            public string ID => Guid.NewGuid().ToString();
            public int Number { get; set; }
            public bool TrueFalse { get; set; }
            public DateTime DateTime { get; set; }
            public IEnumerable<InnerSeed> ArraySeed { get; set; }
        }
        private struct InnerSeed
        {
            public string CaseID => Guid.NewGuid().ToString();
            public string Content { get; set; }
            public int Num { get; set; }
        }

    }

}