﻿using Microsoft.AspNetCore.Mvc.Rendering;
using OutServices.Web.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace OutServices.Web.Helper
{
    using EFModel;
    using Services;
    public static class DataHelper
    {
        private static Master GetCodeData(string code)
        {
            ICodeHandler codeServices = new CodeHandler();
            Master data = codeServices.ReadMasters(new Master { Code = code }).FirstOrDefault();
            if (data != null)
            {
                IEnumerable<Detail> detail = codeServices.ReadDetails(new Detail { Code = code });
                data.Detail = detail.ToList();
            }
            return data;
        }
        internal static IEnumerable<Detail> GetDetails(string code, string value = "")
        {
            ICodeHandler codeServices = new CodeHandler();
            Detail para = new Detail { Code = code, Value = value };
            return codeServices.ReadDetails(para);
        }

        internal static Sns GetSns(Sns sns)
        {
            ISnsHandler snsServices = new SnsHandler();
            return snsServices.Read(account: sns.Account).FirstOrDefault();
        }

        private static IEnumerable<SelectListItem> GetSelecListItem(string code, int selectedValue = -99)
        {
            Master data = GetCodeData(code);
            IEnumerable<SelectListItem> retuSelectListItems = new List<SelectListItem>();
            if (data.Detail != null && data.Detail.Any())
            {
                retuSelectListItems = data.Detail.Select(
                    x => new SelectListItem
                    {
                        Text = x.Text,
                        Value = x.CodeSn.ToString(),
                        Selected = selectedValue >= 0 && x.CodeSn == selectedValue
                    });
            }
            return retuSelectListItems;
        }

        private static IEnumerable<City> GetAllCitys(string code = "")
        {
            ICityHandler cityServices = new CityHandler();
            return cityServices.GetAllCitys();
        }

        private static IEnumerable<SelectListItem> GetAllCitysSelectListItems(string code = "")
        {
            IEnumerable<City> data = GetAllCitys(code);
            return data.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.AdCode
            });
        }

        internal static int AddUser(User user)
        {
            IUserHandler userHandler = new UserHandler();
            return userHandler.Create(user);
        }

        internal static User ReadUser(string id)
        {
            IUserHandler userHandler = new UserHandler();
            return userHandler.Read(id: id, hasChildren: true);
        }

        internal static IEnumerable<User> GetAllAspNetUsers()
        {
            IAspNetUserHandler aspNetUserHandler = new AspNetUserHandler();
            return aspNetUserHandler.GetAllAspNetUsers();
        }

        private static User GetNewUser(string id)
        {
            return new User
            {
                Id = id,
                Sns = new List<Sns>(),
                Phone = new List<Phone>(),
                Organization = new List<Organization>()
            };
        }
        internal static EditViewModel SetUserListData(this EditViewModel model, string aspNetUserId = "")
        {

            if (model == null)
            {
                model = new EditViewModel
                {
                    User = GetNewUser(aspNetUserId),
                    ActionType = ActionType.Update
                };
            }
            else
            {
                if (model.User == null)
                {
                    model.User = GetNewUser(aspNetUserId);
                }
                else
                {
                    model.User = model.User;
                    model.SystemData = model.User.CreateDateTime.HasValue;
                    model.User.Sns = model.User.Sns;
                    model.User.Phone = model.User.Phone;
                    model.User.Organization = model.User.Organization;
                }
            }
            model.CityItems = GetAllCitysSelectListItems();
            model.PhoneType = GetSelecListItem("Phone");
            model.SNSType = GetSelecListItem("SNS");
            model.OrganiztionType = GetSelecListItem("Organization");
            return model;
        }
    }
}
