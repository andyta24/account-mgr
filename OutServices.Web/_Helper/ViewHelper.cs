﻿using System.Collections.Generic;
namespace OutServices.Web.Helper
{
    using ViewModel;
    public static class ViewHelper
    {
        public static Dictionary<string, RenderInfo> GetRenderConfig(string controller = "")
        {
            return GetRenderData(controller);
        }
        private static Dictionary<string, RenderInfo> GetRenderData(string controller = "")
        {
            controller = string.IsNullOrEmpty(controller) ? "Account" : controller;
            return new Dictionary<string, RenderInfo>
            {
                { "List", new RenderInfo
                {
                    RouteValue = 0,
                    MethodType = "Post",
                    Area = "Backend",
                    Action = "RenderPartialView",
                    Controller = controller,
                    PVName = "_List"
                } },
                { "Edit", new RenderInfo
                {
                    RouteValue = 3,
                    MethodType = "Post",
                    Area = "Backend",
                    Action = "RenderPartialView",
                    Controller = controller,
                    PVName = "_Edit"
                } },
                { "Insert", new RenderInfo
                {
                    RouteValue = 1,
                    MethodType = "Post",
                    Area = "Backend",
                    Action = "RenderPartialView",
                    Controller = controller,
                    PVName = "_Edit"
                } },
                {
                    "Remove",new RenderInfo
                    {
                        RouteValue = 2,
                        MethodType = "Post",
                        Area = "Backend",
                        Action = "RemoveUser",
                        Controller = controller,
                        PVName = "_List"
                    }
                }
            };
        }
    }
}
