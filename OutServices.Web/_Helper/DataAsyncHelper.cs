﻿using OutServices.Web.Services.Async;

namespace OutServices.Web.Helper
{
    public class DataAsyncHelper
    {
        public static CodeAsyncHandler CodeAsyncServices() => new CodeAsyncHandler();
        public static UserAsyncHandler UserAsyncServices() => new UserAsyncHandler();
    }
}
