﻿using System.Data;
using System.Data.SqlClient;

namespace OutServices.Web.Services.Repository.Helper
{
    using Microsoft.Extensions.Configuration;
    using System.IO;

    public static class DbHelper
    {
        private const string settingFile = "appsettings.json";

        public static IDbConnection GetDbConnection(string section = null, string key = null)
        {
            string connectionString = GetConfiguration(key: key);
            IDbConnection Connection = new SqlConnection(connectionString);
            if (Connection.State != ConnectionState.Open)
                Connection.Open();
            return Connection;
        }

        //public static IDbConnection GetDbConn(string section = null, string key = null)
        //{
        //    string connectionString = string.IsNullOrEmpty(section) ? GetConfiguration() : section;
        //    string providerName = string.IsNullOrEmpty(key) ? "Systemp.Data.SqlClient" : key;
        //    IDbConnection Connection = new SqlConnection(connectionString);
        //    return Connection;
        //}
        private static string GetConfiguration(string section = null, string key = null)
        {
            section = section ?? "ConnectionStrings";
            key = key ?? "DefaultConnection";
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(settingFile);
            IConfiguration Configuration = builder.Build();
            string jsonKey = $"{section}:{key}";
            string returnConnectionString = Configuration[jsonKey];
            return returnConnectionString;
        }
    }
}
