SELECT *
  FROM AspNetUsers
  LEFT JOIN Authority.[User] ON Authority.[User].Id = dbo.AspNetUsers.Id
  LEFT JOIN Authority.SNS ON Authority.SNS.UserId = Authority.[User].UserId
  LEFT JOIN Authority.Phone ON Authority.Phone.UserId = Authority.[User].UserId
  LEFT JOIN Authority.Organization ON Authority.Organization.UserId = Authority.[User].UserId